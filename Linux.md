# Linux CLI

'#' ==> Sign for root user  
ls: List files & directories.  
ls -a : display hidden files  
ls -l : shows us permissions of files  
cd: Change directory.  
pwd: Print working directory.  
mkdir: Create a new directory.  
rmdir: Delete directory.  
rm: Remove files and directories.  
cp: Copy files and directories.  
mv: Move or rename files and directories.  
cat: Concatenate and display file content.  
grep: Search for a pattern in files.  
sudo: Execute a command with administrative privileges.  
chown: Change file ownership.  
tar: Create or extract compressed archives.  
ssh: Securely access remote servers.  
wget: Download files from the web.  
top: Monitor system processes.  
ps: Display information about running processes.  
kill: Terminate processes.  
man: Display the manual pages for a command.  
history: View command history.  
touch : create file ==> touch fileName  
install ==> sudo apt install 'package-name | app-name'  
su - ==> switch account  
chown : change ownership  
(sudo) chown <username>:<groupname> <filename>  
chgrp : change group  
(sudo) chgrp groupName fileName  

<hr/>

# Permissions

### chown : change ownership

    (sudo) chown <username>:<groupname> <filename>


### chgrp : change group

    (sudo) chgrp groupName fileName


<div>
  <img src="./images/photo_2023-06-12_11-48-09.jpg" width="450" >
</div>

<br />

<div>
  <img src="./images/Screenshot%202023-06-12%20120735.png" width="450" >
</div>

<br />

## Modifying permissions 

<br />


### Remove execute permission
    
    sudo chmod -x folderName
    

###  change group permission 
    (sudo) chmod g-w fileName

### add group permission
    (sudo) chmod g+x fileName

### give user permission to execute file

    (sudo) chmod u+x fileName

### Give permission to other

    (sudo) chmod o+x fileName

### You can change permissions by <br />

    u: user
    g: group
    o: other

    + add
    - remove
