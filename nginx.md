# Nginx commands

## `How to install nginx:`

    sudo apt update
    sudo apt install nginx

## `Creating our website:`
Default page is placed in /var/www/html/ location

    cd /var/www
    sudo mkdir <websitename>
    cd <websitename>
    sudo "${EDITOR:-vi}" index.html

- in most cases "${EDITOR:-vi}" is nano or vim 

Now write your code in your index.html file

    <!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title>Hello, Nginx!</title>
    </head>
    <body>
        <h1>Hello, Nginx!</h1>
        <p>We have just configured our Nginx web server on Ubuntu Server!</p>
    </body>
    </html>

Now you can save your changes and exit the editor by:

    CTRL + X
    SHIFT + Y
    ENTER


## `Setting up virtual host:`

    cd /etc/nginx/sites-enabled
    sudo "${EDITOR:-vi}" <websitename>

Now we're gonna write some configs

    server {
        listen <port ex:81>;
        listen [::]:<port ex:81>;

        server_name <your server name ex: example.ubuntu.com>;

        root /var/www/<websitename>;
        index index.html;

        location / {
                try_files $uri $uri/ =404;
           }
    }

Now you can save your changes and exit the editor by:  

    CTRL + X
    SHIFT + Y
    ENTER

## `To make our site working, simply restart Nginx service:`

    sudo service nginx restart


Now open your browser and navigate to 

    http://localhost:<port ex:81>

## `Configuring multiple applications on same port using nginx reverse proxy based on request urls:`

```
cd /etc/nginx/sites-enabled
sudo "${EDITOR:-vi}" <websitename>
```

add this code to the config file:
```
location /website2 {
        rewrite ^/website2(.*) $1 break;
        proxy_pass "http://127.0.0.1:<port for website2>";
    }
```

so it will look like this
```
server {
    listen <port ex:81>;
    listen [::]:<port ex:81>;

    server_name <your server name ex: example.ubuntu.com>;

    root /var/www/<websitename>;
    index index.html;

    location / {
            try_files $uri $uri/ =404;
    }

    location /website2 {
        rewrite ^/website2(.*) $1 break;
        proxy_pass "http://127.0.0.1:<port for website2>";
    }


}
```

Now open your browser on "http://\<your ip address>:\<port>/\<website2>" 
## Example : 
### http://127.0.0.1:3001/myportfolio  

And you'll find your 2nd website over there