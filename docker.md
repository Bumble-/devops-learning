# `Docker`
## Install DOCKER
First, update your existing list of packages:  
    
    $ sudo apt update

Next, install a few prerequisite packages which let apt use packages over HTTPS:

    $ sudo apt install apt-transport-https ca-certificates curl software-properties-common

Then add the GPG key for the official Docker repository to your system:

    $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

Add the Docker repository to APT sources:

    $ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"

Finally, install Docker:

    $ sudo apt install docker-ce

Docker should now be installed, the daemon started, and the process enabled to start on boot. Check that it’s running:

    $ sudo systemctl status docker

The output should be similar to the following, showing that the service is active and running:

    Output
    ● docker.service - Docker Application Container Engine
        Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
        Active: active (running) since Tue 2020-05-19 17:00:41 UTC; 17s ago
    TriggeredBy: ● docker.socket
        Docs: https://docs.docker.com
    Main PID: 24321 (dockerd)
        Tasks: 8
        Memory: 46.4M
        CGroup: /system.slice/docker.service
                └─24321 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock

<hr />

## Build Docker image :
Build a new Docker image from a Dockerfile.

    docker build [options] <path to Dockerfile>

## Working with Docker Images

To check whether you can access and download images from Docker Hub, type:

    $ docker run hello-world

The output will indicate that Docker in working correctly:

    Output
    Unable to find image 'hello-world:latest' locally
    latest: Pulling from library/hello-world
    0e03bdcc26d7: Pull complete
    Digest: sha256:6a65f928fb91fcfbc963f7aa6d57c8eeb426ad9a20c7ee045538ef34847f44f1
    Status: Downloaded newer image for hello-world:latest

    Hello from Docker!
    This message shows that your installation appears to be working correctly.

    ...

You can search for images available on Docker Hub by using the docker command with the search subcommand. For example, to search for the Ubuntu image, type:

    $ docker search ubuntu

The script will crawl Docker Hub and return a listing of all images whose name match the search string. In this case, the output will be similar to this:

<div style="width: 1300px;">

    Output
    NAME                                                      DESCRIPTION                                     STARS               OFFICIAL            AUTOMATED
    ubuntu                                                    Ubuntu is a Debian-based Linux operating sys…   10908               [OK]
    dorowu/ubuntu-desktop-lxde-vnc                            Docker image to provide HTML5 VNC interface …   428                                     [OK]
    rastasheep/ubuntu-sshd                                    Dockerized SSH service, built on top of offi…   244                                     [OK]
    consol/ubuntu-xfce-vnc                                    Ubuntu container with "headless" VNC session…   218                                     [OK]
    ubuntu-upstart                                            Upstart is an event-based replacement for th…   108                 [OK]
    ansible/ubuntu14.04-ansible                               Ubuntu 14.04 LTS with
    ...

<div style="width: 750px;">

To pull ubuntu image use:

    $ docker pull ubuntu

Check if it was been pulled successfully:

    $ docker images

And the output should look like :

    REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
    ubuntu              latest              1d622ef86b13        3 weeks ago         73.9MB
    hello-world         latest              bf756fb1ae65        4 months ago        13.3kB

Remove an image.

    docker rmi <image>


## Running a Docker Container
To run ubuntu container use:

    $ docker run -it ubuntu

The output will look like:

    Output
    root@d9b100f2f636:/#

Now that means you are in the ubuntu container.

## Run a command inside a running container :
    
    docker exec [options] <container> <command>

## Manage docker containers
To view the active containers use:

    $ docker ps

To view all the containers add '-a' so it will look like:

    $ docker ps -a

To start a stopped container, use docker start, followed by the container ID or the container’s name. Let’s start the Ubuntu-based container with the ID of 1c08a7a0d0e4:

    $ docker start 1c08a7a0d0e4

Now if you try 'docker ps' you will find the ubuntu container becaus it's started running

To stop a container write docker folowwed by the container id or name:

    $ docker stop 1c08a7a0d0e4

To remove a container you can use rm followed by container id or name like this:

    $ docker rm 1c08a7a0d0e4

## Committing Changes in a Container to a Docker Image
Then commit the changes to a new Docker image instance using the following command.

    $ docker commit -m "What you did to the image" -a "Author Name" container_id repository/new_image_name

For example, for the user sammy, with the container ID of d9b100f2f636, the command would be:

    $ docker commit -m "added Node.js" -a "sammy" d9b100f2f636 sammy/ubuntu-nodejs

## Pushing Docker Images to a Docker Repository
To push your image, first log into Docker Hub.

    $ docker login -u docker-registry-username

Note: If your Docker registry username is different from the local username you used to create the image, you will have to tag your image with your registry username. For the example given in the last step, you would type:

    $ docker tag sammy/ubuntu-nodejs docker-registry-username/ubuntu-nodejs

Then you may push your own image using:

    $ docker push docker-registry-username/docker-image-name

To push the ubuntu-nodejs image to the sammy repository, the command would be:

    $ docker push sammy/ubuntu-nodejs

The process may take some time to complete as it uploads the images, but when completed, the output will look like this:

    The push refers to a repository [docker.io/sammy/ubuntu-nodejs]
    e3fbbfb44187: Pushed
    5f70bf18a086: Pushed
    a3b5c80a4eba: Pushed
    7f18b442972b: Pushed
    3ce512daaf78: Pushed
    7aae4540b42d: Pushed

    ...

# Docker compose

### Docker Compose is a tool for defining and running multi-container Docker applications. It uses a YAML file to configure the application's services, networks, and volumes. Here are some commonly used Docker Compose command-line commands

<br/>

## docker-compose up :
    Start the containers defined in the Compose file
    docker-compose up [options]

## docker-compose down :
    Stop and remove the containers defined in the Compose file.
    docker-compose down [options]

## docker-compose build :
    Build or rebuild the services defined in the Compose file.
    docker-compose build [options]

## docker-compose start :
    Start the containers defined in the Compose file.
    docker-compose start [options]

## docker-compose stop :
    Stop the containers defined in the Compose file.
    docker-compose stop [options]

## docker-compose restart :
    Restart the containers defined in the Compose file.
    docker-compose restart [options]

## docker-compose logs :
    Fetch the logs of the containers defined in the Compose file.
    docker-compose logs [options]

## docker-compose ps
    List the running containers defined in the Compose file.
    docker-compose ps [options]

