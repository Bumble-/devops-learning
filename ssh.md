### Steps to configuring SSH on a given server

<br />


#### 1: Connect to the Server

    Log in to the server using the appropriate credentials (e.g., username and password) or through another remote access method, such as a virtual machine

#### 2: Check SSH Installation
    sshd -v

#### 3: Install SSH Server : Ubuntu/Debian

    sudo apt update
    sudo apt install openssh-server

#### 4: Configure SSH Server

    Once the SSH server is installed, you can modify its configuration file to adjust the server settings. The SSH server configuration file is typically located at /etc/ssh/sshd_config.

#### 5: Restart SSH Service Ubuntu/Debian

    sudo service ssh restart

#### 6: Configure Firewall

    If a firewall is active on the server, ensure that it allows incoming SSH connections on the configured SSH port. You may need to modify the firewall rules or open the SSH port specifically

#### 7: Test SSH Connection

    ssh username@server_ip

    Replace username with the appropriate username on the server and server_ip with the server's IP address or domain name. If everything is configured correctly, you should be able to establish an SSH connection.



## `Setup SSH connect to remote server`

<br />

### Generating private & public key
    ssh-keygen -t rsa

    cat id_rsa (private-key)

    ssh <server_name>@<server_ip> -p <port>