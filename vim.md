# Vim editor commands

command mode :  
. default mode  
. you can't edit the text  

insert mode :  
. allows you to enter text  

* Press "i" key = switch to insert mode  
* Pres "esc" key = switch to command mode  

:wq = write file to disk and quit vim  
q! = Quit vim without saving the changes  

swith to command mode and type dd  
dd = Delete entire line  
type d10 = Delete next 10 lines  
type u = undo changes (ctrl + z)  

type A = jump to end of line & switch to insert mode  
type 0:zero = jump to start of the line  
$ = jump to end of the line  

specific line:  
. line 12 for example : 12G = Go to line 12  

search in file :  
. /pattern = search for pattern  
. n = jump to next match  
. N = Search in opposite direction  

Replace:  
. :%s/old/new = Replace old with new  

wp = Quite vim  

vim <fileNAme> == Open file with vim | create file  