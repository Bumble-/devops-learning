# Advanced git commands

## Git Rebase
```
git checkout <branch to rebase>
git rebase <branch to rebase on>
```

<div>
  <img src="./images/rebase.jpeg" width="200" >
</div>

## Git Log

The git log command is one of the most useful Git commands you should be familiar with. When you need to look at your commit history, you use the git log command. The git log command displays the most recent commits as well as the current state of the HEAD.

```
git log
```

<div>
  <img src="./images/log.png" width="500" >
</div>

```
git log --graph
```

<div>
  <img src="./images/log--graph.png" width="500" >
</div>

## Git Diff to Compare Commits

```
git diff commit1 commit2
```

<div>
  <img src="./images/diff.png" width="500" >
</div>

## Git Revert

The revert command allows you to undo changes made to a repository’s commit history on the current branch.

```
git revert <commit id>
```

```
git revert HEAD
```

# Git Stash

git stash allows you to temporarily save changes that you have made to your working copy so that you can switch to a different branch, pull in upstream changes, and switch back to your original branch, at which point you can apply the saved changes.

```
git stash
```

This simple command saves all your code modifications but does not commit them. Instead, it saves them locally on your computer. When you’re ready to continue, you can pull your changes from the stash with this command below:


```
git stash pop
```

# Git Tag

```
git tag <tagname>
```

### Example

```
git tag v1.0.0
```
You can create a tag for a specific commit using:
```
git tag v1.0.0 <commit hash>
```
You can also use the -a flag to annotate the tag with a message:
```
git tag -a v1.0.0 -m "Release version 1.0.0"
```
To push the tags to a remote repository, you can use the git push command with the --tags flag:
```
git push --tags
```
To view the list of tags you made in your repository:
```
git tag --list
```
<div>
  <img src="./images/tag-list.png" width="500" >
</div>
